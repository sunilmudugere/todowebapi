using System.Collections.Generic;
using System.Linq;
using TodoApi.Models;

namespace TodoApi.Contracts
{
    public interface ITodoService
    {
        List<TodoItem> GetAllTodoItems();
        TodoItem GetTodoItemById(int id);

        bool CreateTodoItem(TodoItem item);
    }
}